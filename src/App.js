import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ConfigureStore } from './redux/configureStore';
import Navigation from './navigation/Navigation';

const {store, persistor} = ConfigureStore();

const App = () => {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
            <Navigation/>
          </BrowserRouter>
        </PersistGate>
      </Provider>
    );
}

export default App;

