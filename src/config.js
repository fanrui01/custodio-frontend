const env = process.env.NODE_ENV; // 'development' or 'production' or ...
console.log("env: ", env);

let serverUrl, authServerUrl;

if (env==="production"){
    serverUrl = "https://j1banbbvsb.execute-api.ap-southeast-1.amazonaws.com/dev";
    authServerUrl = "https://j1banbbvsb.execute-api.ap-southeast-1.amazonaws.com/dev";
} else {
    serverUrl = "http://localhost:8000";
    authServerUrl = "http://localhost:8000";
}

export { serverUrl, authServerUrl };