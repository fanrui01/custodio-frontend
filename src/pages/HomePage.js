import React, { Component } from 'react';
import { Container } from 'reactstrap';

import Header from '../components/HeaderComponent';
import TopPart from '../components/TopPartComponent'

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isHeaderDropDownOpen: false
        };

        this.toggleHeaderDropdown = this.toggleHeaderDropdown.bind(this);
    }

    toggleHeaderDropdown() {
        this.setState({
            isHeaderDropDownOpen: !this.state.isHeaderDropDownOpen
        });
    }

    render(){
        var information = {
            "title": "Custodio Technologies Test Case",
            "description": "Test Cover Page.",
            "paragraph": "Simply a cover page, click to load the 'Record' page."
        }
        return(
            <Container>
                <Header auth={this.props.auth} 
                        isDropDownOpen={this.state.isHeaderDropDownOpen}
                        toggle={this.toggleHeaderDropdown}
                        userLogin={this.props.userLogin}
                        userLogout={this.props.userLogout}
                        />
                <TopPart title={information.title} 
                        description={information.description}
                        paragraph={information.paragraph} 
                        />
            </Container>
        );
    }
}

export default Home;   