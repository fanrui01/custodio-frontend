import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import Header from '../components/HeaderComponent';

import RecordCard from '../components/RecordCardComponent'

import apis from '../apis';

class Record extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isRecordsLoaded: false,
            records: [],
            isInterestsLoaded: false,
            interests: [],
            isOccupationsLoaded: false,
            occupations: [],

            recordToUpdate: null,
            recordToAdd: {
                date_of_birth: "1970-01-01",
                gender: "M",
                interests: [],
                name: "",
                occupation: 1
            },

            isHeaderDropDownOpen: false,
            isRecordModalOpen: false,
            isNewRecordModalOpen: false,
        };

        this.toggleHeaderDropdown = this.toggleHeaderDropdown.bind(this);

        this.toggleRecordModal = this.toggleRecordModal.bind(this);
        this.toggleNewRecordModal = this.toggleNewRecordModal.bind(this);

        this.onChangeNameEdit = this.onChangeNameEdit.bind(this)
        this.onChangeDobEdit = this.onChangeDobEdit.bind(this)
        this.onChangeOccupationEdit = this.onChangeOccupationEdit.bind(this)
        this.onChangeInterestsEdit = this.onChangeInterestsEdit.bind(this)
        this.onClickConfirmEdit = this.onClickConfirmEdit.bind(this)

        this.onChangeNameAdd = this.onChangeNameAdd.bind(this)
        this.onChangeGenederAdd = this.onChangeGenederAdd.bind(this)
        this.onChangeDobAdd = this.onChangeDobAdd.bind(this)
        this.onChangeOccupationAdd = this.onChangeOccupationAdd.bind(this)
        this.onChangeInterestsAdd = this.onChangeInterestsAdd.bind(this)
        this.onClickConfirmAdd = this.onClickConfirmAdd.bind(this)
    }

    toggleHeaderDropdown() {
        this.setState({
            isHeaderDropDownOpen: !this.state.isHeaderDropDownOpen
        });
    }

    toggleRecordModal(e) {
        var recordToUpdate = this.state.records.find( element  => element.id.toString()  === e.target.value);

        if (this.state.isRecordModalOpen===true){
            this.setState({
                isRecordModalOpen: !this.state.isRecordModalOpen,
            });
        } else {
            this.setState({
                isRecordModalOpen: !this.state.isRecordModalOpen,
                recordToUpdate: recordToUpdate
            });
        }
    }

    toggleNewRecordModal(e) {
        this.setState({
            isNewRecordModalOpen: !this.state.isNewRecordModalOpen,
        })
    }

    onChangeNameEdit(e) {
        var new_name = e.target.value
        this.setState((prevState) => ({
            recordToUpdate: {
                ...prevState.recordToUpdate, 
                name: new_name, 
            }
        }));
    }

    onChangeDobEdit(e) {
        var new_dob = e.target.value
        this.setState((prevState) => ({
            recordToUpdate: {
                ...prevState.recordToUpdate, 
                date_of_birth: new_dob, 
            }
        }));
    }

    onChangeOccupationEdit(e) {
        console.log("========onChangeOccupationEdit========")

        var new_occupation_id = e.target.value
        this.setState((prevState) => ({
            recordToUpdate: {
                ...prevState.recordToUpdate, 
                occupation: parseInt(new_occupation_id), 
            }
        }));
    }

    onChangeInterestsEdit(e) {
        console.log("========onChangeInterestsEdit========")
        console.log("========e.target.value========")
        console.log(e.target.value)

        let new_interest_ids = Array.from(e.target.selectedOptions, option => option.value);
        new_interest_ids = new_interest_ids.map(id => parseInt(id))
        this.setState((prevState) => ({
            recordToUpdate: {
                ...prevState.recordToUpdate, 
                interests: new_interest_ids, 
            }
        }));
    }

    onClickConfirmEdit(e){
        if (!this.alphanumericalValidator(this.state.recordToUpdate.name) || !this.dateValidator(this.state.recordToUpdate.date_of_birth)){
            return
        }

        var id = this.state.recordToUpdate.id;
        var body = this.state.recordToUpdate;
        delete body.id;

        apis.records.update(id, body, this.props.auth.token).then(() =>{
            console.log("updating.")
            window.location.reload()
        }).catch(err =>{
            console.log("error: ", err)
            window.location.reload()
        })
    }

    onChangeNameAdd(e) {
        var new_name = e.target.value
        this.setState((prevState) => ({
            recordToAdd: {
                ...prevState.recordToAdd, 
                name: new_name, 
            }
        }));
    }
    
    onChangeGenederAdd(e) {
        var new_gender = e.target.value
        this.setState((prevState) => ({
            recordToAdd: {
                ...prevState.recordToAdd, 
                gender: new_gender, 
            }
        }));
    }
    
    onChangeDobAdd(e) {
        var new_dob = e.target.value
        this.setState((prevState) => ({
            recordToAdd: {
                ...prevState.recordToAdd, 
                date_of_birth: new_dob, 
            }
        }));
    }

    onChangeOccupationAdd(e) {
        var new_occupation_id = e.target.value
        this.setState((prevState) => ({
            recordToAdd: {
                ...prevState.recordToAdd, 
                occupation: parseInt(new_occupation_id), 
            }
        }));
    }

    onChangeInterestsAdd(e) {
        console.log("========onChangeInterestsEdit========")
        console.log("========e.target.value========")
        console.log(e.target.value)

        let new_interest_ids = Array.from(e.target.selectedOptions, option => option.value);
        new_interest_ids = new_interest_ids.map(id => parseInt(id))
        this.setState((prevState) => ({
            recordToAdd: {
                ...prevState.recordToAdd, 
                interests: new_interest_ids, 
            }
        }));
    }

    onClickConfirmAdd(e){
        if (!this.alphanumericalValidator(this.state.recordToAdd.name) || !this.dateValidator(this.state.recordToAdd.date_of_birth)){
            return
        }

        var body = this.state.recordToAdd;
        delete body.id;
        apis.records.add(body, this.props.auth.token).then(() =>{
            console.log("Adding.")
            window.location.reload()
        }).catch(err =>{
            console.log("error: ", err)
            window.location.reload()
        })
    }

    componentDidMount(){
        if (this.state.isRecordsLoaded === false){
            // apis.records.list().then(records => {
            apis.records.list(this.props.auth.token).then(records => {
                this.setState({ "records": records, "isRecordsLoaded": true})
            })
            .catch(error => {
                console.log(error);
            });
        }

        if (this.state.isInterestsLoaded === false){
            apis.interests.list().then(interests => {
                this.setState({ "interests": interests, "isRecisInterestsLoadedordsLoaded": true})
                console.log("interests: ", interests)
            })
            .catch(error => {
                console.log(error);
            });
        }

        if (this.state.isOccupationsLoaded === false){
            apis.occupations.list().then(occupations => {
                this.setState({ "occupations": occupations, "isOccupationsLoaded": true})
            })
            .catch(error => {
                console.log(error);
            });
        }
    }

    alphanumericalValidator(str){
        if (str===null || str===undefined) {
            return false
        }
        if (!str.match(/^[0-9a-zA-Z ]+$/)){
            return false
        }
        return true
    }

    dateValidator(str){
        if (str===null || str===undefined) {
            return false
        }
        if (!str.match(/^\d{4}-\d{2}-\d{2}$/)){
            return false;
        }// Invalid format
        var d = new Date(str);
        var dNum = d.getTime();
        if(!dNum && dNum !== 0) return false; // NaN value, Invalid date
        return d.toISOString().slice(0,10) === str;
    }

    onClickDelete(e) {
        apis.records.destroy(e.target.value)
        .catch(error => {
            console.log(error);
            window.location.reload();
        });
    }

    render(){

        console.log("this.state: ", this.state)
        return(
            <Container>
                <Header auth={this.props.auth} 
                        isDropDownOpen={this.state.isHeaderDropDownOpen}
                        toggle={this.toggleHeaderDropdown}
                        userLogin={this.props.userLogin}
                        userLogout={this.props.userLogout}
                        />
                <br />
                {this.props.auth.isAuthenticated ? <Button color="success" onClick={this.toggleNewRecordModal}>New Record</Button> : null }
                <br />
                <Modal isOpen={this.state.isRecordModalOpen} toggle={this.toggleRecordModal} size="lg" >
                    <ModalHeader toggle={this.toggleRecordModal}>Edit</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="editName">Name</Label>
                                <Input name="editName" id="editName" placeholder={ this.state.recordToUpdate === null ? "" : this.state.recordToUpdate.name } onChange={ this.onChangeNameEdit } />
                                { this.state.recordToUpdate!=null && this.alphanumericalValidator(this.state.recordToUpdate.name) === true ? null : <FormText color="danger"> Not alphanumeric </FormText>  }
                            </FormGroup>
                            <FormGroup>
                                <Label for="editDob">Date of Birth (in YYYY-MM-DD)</Label>
                                <Input type="editDob" name="editDob" id="editDob" placeholder={ this.state.recordToUpdate === null ? "" : this.state.recordToUpdate.date_of_birth } onChange={ this.onChangeDobEdit } />
                                { this.state.recordToUpdate!=null && this.dateValidator(this.state.recordToUpdate.date_of_birth) === true ? null : <FormText color="danger"> Not in YYYY-MM-DD format </FormText>  }
                            </FormGroup>
                            <FormGroup>
                                <Label for="editOccupation">Occupation</Label>
                                <Input type="select" name="editOccupation" id="editOccupation" onChange={this.onChangeOccupationEdit}>
                                    { this.state.occupations.map(occupation => <option value={occupation.id} selected={this.state.recordToUpdate!=null && occupation.id===this.state.recordToUpdate.occupation } > {occupation.name} </option> ) }
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="editInterests">Interests</Label>
                                <Input type="select" name="editInterests" id="editInterests" onChange={this.onChangeInterestsEdit} multiple>
                                    { this.state.interests.map(interest => <option value={interest.id} selected={this.state.recordToUpdate!=null && this.state.recordToUpdate.interests.includes(interest.id) }> {interest.name} </option> ) }
                                </Input>
                            </FormGroup>
                            <Button color="primary" onClick={ this.onClickConfirmEdit }> Confirm </Button>
                        </Form>
                    </ModalBody>
                </Modal>
                <br />
                <Modal isOpen={this.state.isNewRecordModalOpen} toggle={this.toggleNewRecordModal} size="lg" >
                    <ModalHeader toggle={this.toggleNewRecordModal}>New</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="addName">Name</Label>
                                <Input name="addName" id="addName" placeholder={ this.state.recordToAdd === null ? "" : this.state.recordToAdd.name } onChange={ this.onChangeNameAdd } />
                                { this.state.recordToAdd!=null && this.alphanumericalValidator(this.state.recordToAdd.name) === true ? null : <FormText color="danger"> Not alphanumeric </FormText>  }
                            </FormGroup>
                            <FormGroup>
                                <Label for="addGender">Gender</Label>
                                <Input type="select" name="addGender" id="addGender" onChange={this.onChangeGenederAdd}>
                                    <option value={"M"} selected={true} > Male </option> 
                                    <option value={"F"} > Female </option> 
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="addDob">Date of Birth (in YYYY-MM-DD)</Label>
                                <Input type="addDob" name="addDob" id="addDob" placeholder={ this.state.recordToAdd === null ? "" : this.state.recordToAdd.date_of_birth } onChange={ this.onChangeDobAdd } />
                                { this.state.recordToAdd!=null && this.dateValidator(this.state.recordToAdd.date_of_birth) === true ? null : <FormText color="danger"> Not in YYYY-MM-DD format </FormText>  }
                            </FormGroup>
                            <FormGroup>
                                <Label for="addOccupation">Occupation</Label>
                                <Input type="select" name="addOccupation" id="addOccupation" onChange={this.onChangeOccupationAdd}>
                                    { this.state.occupations.map(occupation => <option value={occupation.id} selected={this.state.recordToAdd!=null && occupation.id===this.state.recordToAdd.occupation } > {occupation.name} </option> ) }
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="addInterests">Interests</Label>
                                <Input type="select" name="addInterests" id="addInterests" onChange={this.onChangeInterestsAdd} multiple>
                                    { this.state.interests.map(interest => <option value={interest.id} selected={this.state.recordToAdd!=null && this.state.recordToAdd.interests.includes(interest.id) }> {interest.name} </option> ) }
                                </Input>
                            </FormGroup>
                            <Button color="primary" onClick={ this.onClickConfirmAdd }> Confirm </Button>
                        </Form>
                    </ModalBody>
                </Modal>
                <br />
                <Row>
                { this.state.records.map(record => <Col xs="12" sm="6" md="4"><RecordCard 
                                                                    title={record.name + ' (' + (this.state.occupations.find(occupation => occupation.id === record.occupation) !== undefined ? this.state.occupations.find(occupation => occupation.id === record.occupation).name : null) + ')'}
                                                                    subtitle={'Gender: ' + record.gender} 
                                                                    // text={"Interests: "+ record.interests.toString()}
                                                                    text={"Interests: "+ record.interests.map(interest_id => this.state.interests.find(interest => interest.id === interest_id) !== undefined ? this.state.interests.find(interest => interest.id === interest_id).name : null ).toString() }
                                                                    button_text_1={"EDIT"}
                                                                    button_value_1={record.id}
                                                                    button_func_1={ this.toggleRecordModal }
                                                                    button_color_1={"primary"}
                                                                    button_text_2={"DELETE"}
                                                                    button_value_2={record.id}
                                                                    button_func_2={ this.onClickDelete }
                                                                    button_color_2={"danger"}
                                                                        /></Col>)}
                </Row>
            </Container>
        );
    }
}

export default Record;   