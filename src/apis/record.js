import { serverUrl } from '../config';



const list = async (token) => {
    return fetch(serverUrl + '/info/records/', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "JWT " + token
                }
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
                },
                error => {
                    var errmsg = new Error(error.message);
                    throw errmsg;
                }
            )

    
}

const update = async (id, body, token) => {
    return fetch(serverUrl + '/info/records/' + id.toString() + "/", {
                method: "PATCH",
                body: JSON.stringify(body),
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "JWT " + token
                }
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
                },
                error => {
                    var errmsg = new Error(error.message);
                    throw errmsg;
                }
            )
}

const destroy = async (id, token) => {
    return fetch(serverUrl + '/info/records/' + id.toString() + "/", {
                method: "DELETE",
                headers: {
                    "Authorization": "JWT " + token
                }
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
                },
                error => {
                    var errmsg = new Error(error.message);
                    throw errmsg;
                }
            )
}


const add = async (body, token) => {
    console.log("body to post: ", body)
    return fetch(serverUrl + '/info/records/', {
                method: "POST",
                body: JSON.stringify(body),
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "JWT " + token
                }
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
                },
                error => {
                    var errmsg = new Error(error.message);
                    throw errmsg;
                }
            )
}

export default { list, update, destroy, add };



