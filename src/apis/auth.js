import { authServerUrl } from '../config';

const login = (username, password) => {

    const loginCredential = {
        "username": username,
        "password": password
    }

    return fetch(authServerUrl + '/users/auth/login/', {
        method: "POST",
        body: JSON.stringify(loginCredential),
        headers: {
            "Content-Type": "application/json",
        }
    })
    .then(response => {
        if (response.ok) {
            return response.json();
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
}

const logout = (token) => {

    return fetch(authServerUrl + '/users/auth/logout/', {
        method: "POST",
        mode: 'cors', 
        headers: {
            "Content-Type": "application/json",
            "Authorization": "JWT " + token
        }
    })
    .then(response => {
            if (response.ok) {
                console.log("Logout: reponse is ok.")
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            console.log("errmess");
            console.log(errmess);
            throw errmess;
        })
}

const verifyToken = (token) => {

    const data = {
        "token": token
    }

    return fetch(authServerUrl + '/users/token-verify/', {
                method: "POST",
                mode: 'cors', 
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
                },
                error => {
                    var errmess = new Error(error.message);
                    throw errmess;
                })
}

export default {login, logout, verifyToken, }