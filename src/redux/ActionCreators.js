import * as ActionTypes from './ActionTypes';

import apis from '../apis';

export const userLogin = (username, password) => (dispatch) => {

    dispatch(userLoginLoading());

    return apis.auth.login(username, password)
            .then(auth => dispatch(userLoginSuccess(auth.token)))   
            .catch(error => dispatch(userLoginFailed(error.message)));
}

const userLoginLoading = () => ({
    type: ActionTypes.LOGIN_LOADING
});

const userLoginFailed = (errmess) => ({
    type: ActionTypes.LOGIN_FAILED,
    payload: errmess
});

const userLoginSuccess = (token) => ({
    type: ActionTypes.LOGIN,
    payload: token
});

export const userLogout = (token) => (dispatch) => {

    dispatch(userLogoutLoading(true));

    return apis.auth.logout(token)
            .then(auth => dispatch(userLogoutSuccess())) 
            .catch(error => dispatch(userLogoutFailed(error.message)));
}


const userLogoutLoading = () => ({
    type: ActionTypes.LOGOUT_LOADING
});

const userLogoutFailed = (errmess) => ({
    type: ActionTypes.LOGOUT_FAILED,
    payload: errmess
});

const userLogoutSuccess = () => {
    return {
        type: ActionTypes.LOGOUT
    }
};

export const verifyToken = (token) => (dispatch) => {

    return apis.auth.verifyToken(token)
            .then(auth => dispatch(userLoginSuccess(auth.token)))   
            .catch(error => dispatch(userLogoutSuccess(error.message)));
}